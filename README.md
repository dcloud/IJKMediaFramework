# ijkplayer DCloud 定制版

iOS 用的版本

git 地址: https://github.com/bilibili/ijkplayer

因为没有指定的tag  目前用的是最新的master 分支 
commitID 30eb9441945da795079492041a791c121d2b8206


ffmpeg升级到4.0
ffmpeg: ff4.0--ijk0.8.8--20210426--001


## 以后升级需要修改的东西

1. config/module-lite-hevc.sh  

注释或者删除掉以下两项
```
  #export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --disable-ffserver"
  #export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --disable-vda"
```

然后在末尾添加以下几行
```
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-protocol=rtp"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-demuxer=rtsp"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-demuxer=sdp"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-demuxer=rtp"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-protocol=tcp"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-decoder=mjpeg"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-demuxer=mjpeg"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --enable-decoder=mjpeg4"
export COMMON_FF_CFG_FLAGS="$COMMON_FF_CFG_FLAGS --disable-bsf=eac3_core"
```

2. 修改 `init-config.sh`
  ```
  rm -rf config/module.sh
  if [ ! -f 'config/module.sh' ]; then
    cp config/module-lite-hevc.sh config/module.sh
  fi
  ```
  执行 `./init-config.sh`

3. 更改最低支持的版本
   
    因为ffmpeg4.0 使用的部分API是iOS8.0以上的，所以这里必须改一下最低支持的版本，不然编译ffmpeg会出现类似的错误

    ```
    libavcodec/videotoolbox.c:862:9: error: 'VTDecompressionSessionInvalidate' is only available on iOS 8.0 or newer
      [-Werror,-Wunguarded-availability]
        VTDecompressionSessionInvalidate(videotoolbox->session);

    ```
    修改 `ios/tools/do-compile-ffmpeg.sh` 和  `ios/tools/do-compile-openssl.sh`

    
    找到相关 `version-min` 小于8.0的都改成8.0 (因为uni-app iOS支持最低版本目前为8.0)

4. 修改 `init-ios-openssl.sh`
   注释掉 armv7s 和 i386
```
	pull_fork "armv7"
	# pull_fork "armv7s"
	pull_fork "arm64"
	# pull_fork "i386"
	pull_fork "x86_64"
    
```

  执行 `init-ios-openssl.sh`

  
  修改 `init-ios.sh` 这一行去掉i386
  ```
  FF_ALL_ARCHS_IOS8_SDK="armv7 arm64 x86_64"
  ```

   执行 `init-ios.sh`


5. cd 到 `ios`目录

   依次执行
   
   ```
   ./compile-ffmpeg.sh clean
   ./compile-ffmpeg.sh all
   ./compile-openssl.sh clean
   ./compile-openssl.sh all
   
   ```


6. 打开 `IJKMediaPlayer.xcodeproj`
  找到 `IJKMediaFrameworkWithSSL.framework`  Target 
  检查一下  `libcrypto.a`, `libssl.a` 这两个是否在库依赖中 

  然后 build  真机和模拟器 然后lipo 成一个静态库


## uniapp 修改记录
 2022.07.26
 修改 `IJKAudioKit.m`,`IJKAVMoviePlayerController.m`,`IJKFFMoviePlayerController`
主要修改内容为 注册掉  `AVAudioSessionInterruptionNotification `的监听
因为会打断其他后台播放音频 [ask帖子](https://ask.dcloud.net.cn/question/146719)

